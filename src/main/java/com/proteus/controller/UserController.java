package com.proteus.controller;

import com.proteus.model.User;
import com.proteus.service.AutomaticStatusUpdater;
import com.proteus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;

    private final AutomaticStatusUpdater automaticStatusUpdater;

    @Autowired
    public UserController(UserService userService, AutomaticStatusUpdater automaticStatusUpdater) {
        this.userService = userService;
        this.automaticStatusUpdater = automaticStatusUpdater;
    }

    @GetMapping(value = "/{id}")
    public User findById(@PathVariable long id){
        return userService.findById(id);
    }

    @PostMapping
    public long save(@RequestBody User user){
        userService.save(user);
        if(user.getStatus().equals("Online"))
            automaticStatusUpdater.updateUserStatus(user);
        return user.getId();
    }

    @PatchMapping(value = "/{id}")
    public Map<String, String> updateStatus(@PathVariable long id, @RequestBody String status ) {
        User user = userService.findById(id);
        String previousStatus;
        Map<String, String> map = new LinkedHashMap<>();
        if (!user.isEmpty()) {
            previousStatus = user.getStatus();
            user.setStatus(status);
            userService.save(user);
            if (user.getStatus().equals("Online"))
                automaticStatusUpdater.updateUserStatus(user);
            map.put("id", String.valueOf(id));
            map.put("previous-status", previousStatus);
            map.put("new-status", status);
        }
        return map;
    }
}
