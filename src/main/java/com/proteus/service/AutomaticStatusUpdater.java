package com.proteus.service;

import com.proteus.model.User;

public interface AutomaticStatusUpdater {
    void updateUserStatus(User user);
}
