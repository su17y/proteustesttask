package com.proteus.service.impl;

import com.proteus.dao.UserRepository;
import com.proteus.model.User;
import com.proteus.service.AutomaticStatusUpdater;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.Map;
import java.util.concurrent.*;

@Service
public class AutomaticStatusUpdaterImpl implements AutomaticStatusUpdater {

    private final ScheduledExecutorService executorService = Executors
            .newSingleThreadScheduledExecutor();

    private Map<User,ScheduledFuture> map = new ConcurrentHashMap<>();

    private final UserRepository userRepository;

    public AutomaticStatusUpdaterImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void updateUserStatus(User user) {
        user.setStatus("Away");
        Runnable runnableTask = () -> userRepository.save(user);
        ScheduledFuture future = executorService.schedule(runnableTask,5, TimeUnit.MINUTES);
        if(map.containsKey(user)){
            map.get(user).cancel(true);
        }
        map.put(user,future);
    }

    @PreDestroy
    public void destroyMethod(){
        executorService.shutdownNow();
    }
}
