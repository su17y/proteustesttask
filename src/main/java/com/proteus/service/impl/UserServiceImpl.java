package com.proteus.service.impl;

import com.proteus.dao.UserRepository;
import com.proteus.model.User;
import com.proteus.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findById(long id) {
        return userRepository.findById(id).orElse(new User());
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }
}
