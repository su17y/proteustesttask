package com.proteus.service;

import com.proteus.model.User;

public interface UserService {

    User findById(long id);

    void save(User user);
}
