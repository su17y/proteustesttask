DROP TABLE IF EXISTS USER;

CREATE TABLE USER
(
    id    INT PRIMARY KEY AUTO_INCREMENT,
    name  VARCHAR(250) NOT NULL,
    surname  VARCHAR(250) NOT NULL,
    email  VARCHAR(250) NOT NULL,
    phone_number  VARCHAR(250) NOT NULL,
    status  VARCHAR(250) NOT NULL
);

INSERT INTO USER(id, name, surname, email, phone_number, status)
VALUES (1, 'Ivan', 'Ivanov', 'qwerty@gmail.com', '780-380', 'Offline');

